FROM i386/ubuntu:16.04
LABEL description="Preconfigured build environment for Digital Paintball 2"

RUN export DEBIAN_FRONTEND=noninteractive TZ=Europe/London;\
    apt-get update -y; apt-get upgrade -y;\
    apt-get install subversion -y;\
    apt-get install build-essential -y;\
    apt-get install libsdl1.2-dev -y;\
    apt-get install libjpeg62-dev -y;\
    apt-get install libasound2-dev -y;\
    apt-get install ca-certificates -y;\
    useradd -ms /bin/bash dpbuilder;\
    mkdir /code/;\
    mkdir /build/;\
    chown -hR dpbuilder:dpbuilder /code/;\
    chown -hR dpbuilder:dpbuilder /build/;\
    ln -s /build/ /home/dpbuilder/paintball2;\
    chown -hR dpbuilder:dpbuilder /home/dpbuilder/paintball2

ADD builder.sh /usr/bin/
USER dpbuilder
WORKDIR /code/

ENTRYPOINT ["builder.sh"]
