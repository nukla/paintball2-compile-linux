#!/bin/bash
# Script that does various things related to building Digital Paintball 2, like downloading the code, launching a shell inside docker, or just starting the compilation

case "$1" in
"make")
    (
        cd paintball2-code/trunk/paintball2/
        if [ -z "$2" ]; then
            make
        else
            make "$2"
        fi
    )
    ;;
"clean")
    (
        cd paintball2-code/trunk/paintball2/
        make clean
    )
    ;;
"download")
    svn checkout https://svn.code.sf.net/p/paintball2/code/ paintball2-code
    ;;
"shell")
    bash
    ;;
*)
    echo "Usage: \"$0\" <argument>

Possible arguments:
download            Download the source code
make <target>       Build the source code. Just runs 'make <target>' in the container
clean               Remove the compiled binaries. Just runs 'make clean'
shell               Launch an interactive shell (bash) in the container"
    ;;
esac
